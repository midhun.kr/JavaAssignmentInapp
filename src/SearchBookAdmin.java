

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/SearchBookAdmin")
public class SearchBookAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	String url="jdbc:postgresql://localhost/librarymanagement";
	String user="postgres";
	String pas="fermions";
    public SearchBookAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		
		try {
			int parameterintValue;
			Class.forName("org.postgresql.Driver");
			Connection con=DriverManager.getConnection(url,user,pas);
			String paramType=request.getParameter("parameter");
			String paramValue=request.getParameter("parametervalue");
		
			PreparedStatement ps;
			if(paramType.equals("BookName")) {
				ps=con.prepareStatement("select * from book where book_name=?");	
				ps.setString(1,paramValue);
			}else if(paramType.equals("AuthorName")){
				 ps=con.prepareStatement("select * from book where author_name=?");
				 ps.setString(1,paramValue);
			}else{
				 ps=con.prepareStatement("select * from book where book_id=?");
				 ps.setInt(1,Integer.parseInt(paramValue));
			}
		
			ResultSet rs=ps.executeQuery();
			if(rs.next()) {
				out.print("<html><body bgcolor=#86a2cf>");
				out.print("<h2>Book found.<h2>");
				out.print("<table border=1>");
				out.print("<tr>");
				out.print("<th>Book Id</th>");
				out.print("<th> Book Name</th>");
				out.print("<th>Author Name</th>");
				out.print("<th> Category</th>");
				out.print("<th>Year</th>");
				out.print("<th>status</th>");
				parameterintValue=Integer.parseInt(rs.getString(1));
				out.print("</tr>");
				out.print("<tr>");
				out.print("<td>"+rs.getString(1)+"</td>");
				out.print("<td>"+rs.getString(2)+"</td>");
				out.print("<td>"+rs.getString(3)+"</td>");
				out.print("<td>"+rs.getString(4)+"</td>");
				out.print("<td>"+rs.getString(5)+"</td>");
				out.print("<td>"+rs.getString(6)+"</td>");
				
				out.print("</tr></table>");
				
				out.print("</html></body>");
			}else {
				out.print("Book not found!!");
			}
		}catch(Exception e){out.print(e);}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
