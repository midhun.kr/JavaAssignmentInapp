

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/ConfirmIssue")
public class ConfirmIssue extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String url="jdbc:postgresql://localhost/librarymanagement";
	String user="postgres";
	String pas="fermions";   
   
    public ConfirmIssue() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		try {
			
			String startDate=request.getParameter("date_issued");
			String endDate=request.getParameter("due_date");
			
			
			
			Class.forName("org.postgresql.Driver");
			Connection con=DriverManager.getConnection(url,user,pas);
//			
//		
			PreparedStatement ps=con.prepareStatement("insert into issue_ledger values(?,?,?,?)");
			
			ps.setInt(1, Integer.parseInt(request.getParameter("bookid")));
			ps.setString(4, request.getParameter("username"));
			ps.setDate(2,java.sql.Date.valueOf(startDate));
			ps.setDate(3,java.sql.Date.valueOf(endDate));
			int i=ps.executeUpdate();
			PreparedStatement ps2=con.prepareStatement("delete from request_book where book_id=?");
			ps2.setInt(1, Integer.parseInt(request.getParameter("bookid")));
			int j=ps2.executeUpdate();
			PreparedStatement ps3=con.prepareStatement("update book set status=? where book_id=?");
			ps3.setString(1, "issued");
			ps3.setInt(2,Integer.parseInt(request.getParameter("bookid")) );
			int k=ps3.executeUpdate();
			if(i>0&&j>0) {
				if(k>0) {
					out.print("Book Issued");
				}
				
				
				out.print(request.getParameter("date_issued"));
			}
		}catch(Exception e){out.print(e);}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
